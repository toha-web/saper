const page = document.querySelector(".wrapper");
page.addEventListener("click", (e) => {e.preventDefault()});
page.addEventListener("contextmenu", (e) => {e.preventDefault()});

document.querySelector(".copyright a").addEventListener("click", (e) => {
    location.replace(`${e.target.href}`)
});


let recordsData = {
    easy: [],
    normal: [],
    hard: [],
    suicide: []
};
if(localStorage.recordsData){
    try {
        recordsData = JSON.parse(localStorage.getItem("recordsData"));
    } catch (error) {
        console.warn(error);
    }
}

let gameStatus,
gameTime,
t,
lastRecord,
cellCounter;

const fieldBody = document.querySelector(".game-field-body");
const scoreField = document.querySelector(".score");

const timer = document.querySelector(".timer");

const button = document.querySelector(".game-status button");
button.addEventListener("click", () => {
    if (gameStatus === "game"){
        gameTimer("pause");
        button.innerText = "Resume";
        gameStatus = "paused";
    }
    else if(gameStatus === "paused"){
        gameTimer("start");
        button.innerText = "Pause";
        gameStatus = "game";
    }
    else if(gameStatus === "finished"){
        button.innerText = "Pause";
        gameTime = 0;
        timer.innerText = gameTime;
        start();
    }
});

function gameTimer(action) {
    if(action === "start"){
        t = setInterval(() => {
            gameTime += 1;
            timer.innerText = gameTime;
        }, 1000);
    }
    else if(action === "pause" || action === "stop"){
        clearInterval(t);
    }
}

function start(){
    gameStatus = null;
    gameTime = 0;
    timer.innerText = gameTime;
    button.innerText = "Pause";
    button.disabled = true;
    if(localStorage.level){
        let paramsFromStorage = JSON.parse(localStorage.getItem("level"));
        createField(paramsFromStorage.level, paramsFromStorage.bombIndex);
    }
    else{
        createField();
    }
}
start();

function startGame(){
    gameStatus = "game";
    button.disabled = false;
    gameTimer("start");
}

function finishGame(){
    gameStatus = "finished";
    button.innerText = "Start";
    gameTimer("stop");
}

const select = document.querySelector("select");
select.addEventListener("change", () => {
    finishGame();
    let level, bombIndex;
    switch(select.value){
        case "junior": level = 10;
                        bombIndex = 7;
        break;
        case "middle": level = 15;
                        bombIndex = 6;
        break;
        case "senior": level = 20;
                        bombIndex = 5;
        break;
        case "master": level = 30;
                        bombIndex = 4;
        break;
    }
    createField(level, bombIndex);
    let paramsToStorage = {level, bombIndex}
    localStorage.setItem("level", JSON.stringify(paramsToStorage));
    start();
});

function createField(lv = 10, bi = 7){
    fieldBody.replaceChildren();
    let field = [];
    let bombs = [];
    const x = lv;
    const y = Math.round(lv / 1.5);
    const fieldSize = x * y;
    cellCounter = fieldSize;
    const bombCount = fieldSize / bi;
    scoreField.innerText = bombCount;
    for(let i = 1; i <= x; i++){
        field[i] = [];
        for(let j = 0; j < y; j++){
            const cell = document.createElement("div");
            cell.classList.add("cell", "closed");

            cell.bombQty = 0;
            cell.counter = 0;
            cell.flagQty = 0;

            cell.addEventListener("click", () => {
                if(gameStatus === null){
                    startGame();
                }
                else if(gameStatus === "paused" || gameStatus === "finished"){
                    return;
                }

                if(cell.verified || cell.dataset.checked) return;

                if(cell.mined){
                    gameCheck(field);
                }
                else if(!cell.mined){
                    roundCheck(field, cell);
                }
            });

            cell.addEventListener("contextmenu", (e) => {
                e.preventDefault();

                if (gameStatus === null) {
                    startGame();
                }
                else if(gameStatus === "paused" || gameStatus === "finished"){
                    return;
                }

                if(cell.classList.contains("opened")){
                    return;
                }

                if(!cell.dataset.checked){
                    cell.dataset.checked = true;
                    cellCounter--;
                    let temp = scoreField.innerText;
                    --temp;
                    scoreField.innerText = temp;
                    if(temp === 0 && cellCounter === 0){
                        gameCheck(field);
                    }
                }
                else if(cell.dataset.checked){
                    delete cell.dataset.checked;
                    cellCounter++;
                    let temp = scoreField.innerText;
                    ++temp;
                    scoreField.innerText = temp;
                }
            });

            cell.addEventListener("dblclick", () => {
                if(gameStatus === "paused" || gameStatus === "finished"){
                    return;
                }

                if(cell.bombQty !== 0){
                    roundCheck(field, cell, "dblclick");
                }
                else return;
            });

            field[i][j] = cell;
            fieldBody.append(cell);
            fieldBody.style.width = 25 * x + "px";
            fieldBody.style.height = 25 * y + "px";
        }
    }
    bombs = miningField(field, bombCount);
}

function miningField(field, bombCount){
    let bombs = 0;
    let bombAddr = [];
    while(bombs < bombCount){
        let x = Math.floor(Math.random() * (field.length - 1) + 1);
        let y = Math.floor(Math.random() * field[1].length);
        if(!field[x][y].mined){
            field[x][y].mined = true;            
            bombAddr.push(field[x][y]);
            // console.log(field[x][y]);
            bombs++;
        }
        else{
            continue;
        }
    }
    return bombAddr;
}

/**
 * Функция которая проходит по ячейкам находящимся вокруг выбранной ячейки, в зависимости от условий и параметров выполняет разные действия
 * @param {array} field Массив ячеек игрового поля
 * @param {object} cell Ячейка вокруг которой происходит проверка
 * @param {string} flag Необязательный параметр, указывает как именно должна отработать функция
 */
function roundCheck(field, cell, flag = false){
    cell.counter++;
    if(!flag && cell.counter > 2) return;

    if(flag === "dblclick"){
        cell.flagQty = 0;
    }

    find:
    for(let i = 1; i < field.length; i++){
        for(let j = 0; j < field[i].length; j++){
            if(field[i][j] === cell){
                if(i !== 1 && j !== 0){
                    if(flag === "open"){
                        if (!field[i - 1][j - 1].dataset.checked && !field[i - 1][j - 1].verified) {
                            if (field[i - 1][j - 1].mined) {
                                gameCheck(field);
                            } else if (!field[i - 1][j - 1].mined) {
                                roundCheck(field, field[i - 1][j - 1]);
                            }
                        }
                    }
                    else if(flag === "dblclick"){
                        if (field[i - 1][j - 1].dataset.checked) {
                            cell.flagQty++;
                        }
                    }
                    else if(cell.verified){
                        if (!field[i - 1][j - 1].dataset.checked) {
                            cell = field[i - 1][j - 1];
                            if (!cell.verified) {
                                roundCheck(field, cell);
                            }
                        }
                    }
                    else{
                        if(field[i - 1][j - 1].mined){
                            cell.bombQty++;
                        }
                    }
                }
                if(j !== 0){
                    if(flag === "open"){
                        if (!field[i][j - 1].dataset.checked && !field[i][j - 1].verified) {
                            if (field[i][j - 1].mined) {
                                gameCheck(field);
                            } else if (!field[i][j - 1].mined) {
                                roundCheck(field, field[i][j - 1]);
                            }
                        }
                    }
                    else if(flag === "dblclick"){
                        if (field[i][j - 1].dataset.checked) {
                            cell.flagQty++;
                        }
                    }
                    else if(cell.verified){
                        if (!field[i][j - 1].dataset.checked) {
                            cell = field[i][j - 1];
                            if (!cell.verified) {
                                roundCheck(field, cell);
                            }
                        }
                    }
                    else{
                        if(field[i][j - 1].mined){
                            cell.bombQty++;
                        }
                    }
                }
                if(i !== field.length - 1 && j !== 0){
                    if(flag === "open"){
                        if (!field[i + 1][j - 1].dataset.checked && !field[i + 1][j - 1].verified) {
                            if (field[i + 1][j - 1].mined) {
                                gameCheck(field);
                            } else if (!field[i + 1][j - 1].mined) {
                                roundCheck(field, field[i + 1][j - 1]);
                            }
                        }
                    }
                    else if(flag === "dblclick"){
                        if (field[i + 1][j - 1].dataset.checked) {
                            cell.flagQty++;
                        }
                    }
                    else if(cell.verified){
                        if (!field[i + 1][j - 1].dataset.checked) {
                            cell = field[i + 1][j - 1];
                            if (!cell.verified) {
                                roundCheck(field, cell);
                            }
                        }
                    }
                    else{
                        if(field[i + 1][j - 1].mined){
                            cell.bombQty++;
                        }
                    }
                }
                if(i !== 1){
                    if(flag === "open"){
                        if (!field[i - 1][j].dataset.checked && !field[i - 1][j].verified) {
                            if (field[i - 1][j].mined) {
                                gameCheck(field);
                            } else if (!field[i - 1][j].mined) {
                                roundCheck(field, field[i - 1][j]);
                            }
                        }
                    }
                    else if(flag === "dblclick"){
                        if (field[i - 1][j].dataset.checked) {
                            cell.flagQty++;
                        }
                    }
                    else if(cell.verified){
                        if (!field[i - 1][j].dataset.checked) {
                            cell = field[i - 1][j];
                            if (!cell.verified) {
                                roundCheck(field, cell);
                            }
                        }
                    }
                    else{
                        if(field[i - 1][j].mined){
                            cell.bombQty++;
                        }
                    }
                }
                if(i !== field.length - 1){
                    if(flag === "open"){
                        if (!field[i + 1][j].dataset.checked && !field[i + 1][j].verified) {
                            if (field[i + 1][j].mined) {
                                gameCheck(field);
                            } else if (!field[i + 1][j].mined) {
                                roundCheck(field, field[i + 1][j]);
                            }
                        }
                    }
                    else if(flag === "dblclick"){
                        if (field[i + 1][j].dataset.checked) {
                            cell.flagQty++;
                        }
                    }
                    else if(cell.verified){
                        if (!field[i + 1][j].dataset.checked) {
                            cell = field[i + 1][j];
                            if (!cell.verified) {
                                roundCheck(field, cell);
                            }
                        }
                    }
                    else{
                        if(field[i + 1][j].mined){
                            cell.bombQty++;
                        }
                    }
                }
                if(i !== 1 && j !== field[i].length - 1){
                    if(flag === "open"){
                        if (!field[i - 1][j + 1].dataset.checked && !field[i - 1][j + 1].verified) {
                            if (field[i - 1][j + 1].mined) {
                                gameCheck(field);
                            } else if (!field[i - 1][j + 1].mined) {
                                roundCheck(field, field[i - 1][j + 1]);
                            }
                        }
                    }
                    else if(flag === "dblclick"){
                        if (field[i - 1][j + 1].dataset.checked) {
                            cell.flagQty++;
                        }
                    }
                    else if(cell.verified){
                        if (!field[i - 1][j + 1].dataset.checked) {
                            cell = field[i - 1][j + 1];
                            if (!cell.verified) {
                                roundCheck(field, cell);
                            }
                        }
                    }
                    else{
                        if(field[i - 1][j + 1].mined){
                            cell.bombQty++;
                        }
                    }
                }
                if(j !== field[i].length - 1){
                    if(flag === "open"){
                        if (!field[i][j + 1].dataset.checked && !field[i][j + 1].verified) {
                            if (field[i][j + 1].mined) {
                                gameCheck(field);
                            } else if (!field[i][j + 1].mined) {
                                roundCheck(field, field[i][j + 1]);
                            }
                        }
                    }
                    else if(flag === "dblclick"){
                        if (field[i][j + 1].dataset.checked) {
                            cell.flagQty++;
                        }
                    }
                    else if(cell.verified){
                        if (!field[i][j + 1].dataset.checked) {
                            cell = field[i][j + 1];
                            if (!cell.verified) {
                                roundCheck(field, cell);
                            }
                        }
                    }
                    else{
                        if(field[i][j + 1].mined){
                            cell.bombQty++;
                        }
                    }
                }
                if(i !== field.length - 1 && j !== field[i].length - 1){
                    if(flag === "open"){
                        if (!field[i + 1][j + 1].dataset.checked && !field[i + 1][j + 1].verified) {
                            if (field[i + 1][j + 1].mined) {
                                gameCheck(field);
                            } else if (!field[i + 1][j + 1].mined) {
                                roundCheck(field, field[i + 1][j + 1]);
                            }
                        }
                    }
                    else if(flag === "dblclick"){
                        if (field[i + 1][j + 1].dataset.checked) {
                            cell.flagQty++;
                        }
                    }
                    else if(cell.verified){
                        if (!field[i + 1][j + 1].dataset.checked) {
                            cell = field[i + 1][j + 1];
                            if (!cell.verified) {
                                roundCheck(field, cell);
                            }
                        }
                    }
                    else{
                        if(field[i + 1][j + 1].mined){
                            cell.bombQty++;
                        }
                    }
                }

                if(flag === "dblclick"){
                    if(cell.flagQty === cell.bombQty){
                        roundCheck(field, cell, "open");
                    }
                    else return;
                }

                cell.classList.remove("closed");
                cell.classList.add("opened");
                if(!cell.verified){
                    cellCounter--;
                }
                cell.verified = true;
                
                if(cell.bombQty !== 0){
                    cell.innerText = cell.bombQty;
                }
                
                if(cell.bombQty === 0){
                    roundCheck(field, cell);
                }

                break find;
            }
        }
    }
    if(cellCounter === 0 && scoreField.innerText == 0){
        gameCheck(field);
    }
}

function gameCheck(field){
    let gameCheckStatus = true;
    for(let i = 1; i <= (field.length - 1); i++){
        for(let j = 0; j <= (field[i].length - 1); j++){
            if (field[i][j].dataset.checked && !field[i][j].mined){
                field[i][j].classList.add("wrong");
                gameCheckStatus = false;
            }
            else if(field[i][j].mined && !field[i][j].dataset.checked){
                field[i][j].classList.remove("closed");
                field[i][j].classList.add("opened");
                field[i][j].dataset.mined = true;
                gameCheckStatus = false;
            }
        }
    }
    if(gameCheckStatus){
        victory();
    }
    else{
        fail();
    }
    finishGame();
}

function victory(){
    const block = document.querySelector(".finish-victory");
    block.style.height = parseInt(fieldBody.style.height) + 140 + "px";
    block.style.width = fieldBody.style.width;
    block.style.display = "flex";
    block.addEventListener("click", () => {
        block.style.display = "none";
    });
    checkRecords();
}

function fail(){
    const block = document.querySelector(".finish-fail");
    block.style.height = parseInt(fieldBody.style.height) + 140 + "px";
    block.style.width = fieldBody.style.width;
    block.style.display = "flex";
    block.addEventListener("click", () => {
        block.style.display = "none";
    });
}

const recordsIcon = document.querySelector(".records");
const infoSpan = document.createElement("span");
recordsIcon.addEventListener("click", () => {
    const recordsTable = document.querySelector(".records-table");
    recordsTable.style.display = "block";

    const close = document.querySelector(".close-table span");
    close.addEventListener("click", () => {
        recordsTable.style.display = "none";
    });

    const tableBody = document.querySelector(".records-table tbody");
    tableBody.replaceChildren();

    infoSpan.innerText = "Start playing to get into the table!";
    infoSpan.remove();

    if(!localStorage.recordsData){ 
        recordsTable.append(infoSpan);
    }
    else{
        Object.entries(recordsData).forEach(entrie => {
            let level;
            level = entrie[0][0].toUpperCase() + entrie[0].slice(1);
            entrie[1].forEach(rec => {
                const tr = document.createElement("tr");
                const tdLevel = document.createElement("td");
                tdLevel.innerText = `${level}`
                const tdName = document.createElement("td");
                tdName.innerText = `${rec.name}`;
                const tdTime = document.createElement("td");
                let s = rec.time;
                let m = Math.floor(s / 60);
                tdTime.innerText = `${m}:${(s % 60).toString().padStart(2, "0")}`;
                tr.append(tdLevel, tdName, tdTime);
                tableBody.append(tr);
            });
        });
    }
});

function checkRecords(){
    let level;
    if(localStorage.level){
        let levelIndex = JSON.parse(localStorage.getItem("level")).level;
        switch(levelIndex){
            case 10: level = "easy";
            break;
            case 15: level = "normal";
            break;
            case 20: level = "hard";
            break;
            case 30: level = "suicide";
            break;
            default: level = "undefined";
        }
    }
    else{
        level = "easy";
    }

    if(recordsData[level].length > 2){
        if(recordsData[level].at(-1).time > gameTime){
            saveRecords(level);
            return;
        }
    }
    else{
        saveRecords(level);
        return;
    }
}

async function saveRecords(level){
    let name;
    name = await getUserName();
    localStorage.setItem("user", JSON.stringify(name));
    lastRecord = { name, time: gameTime };
    recordsData[level].push({name, time: gameTime});
    recordsData[level].sort((a, b) => {
        return a.time - b.time;
    });
    recordsData[level] = recordsData[level].slice(0, 3);
    localStorage.setItem("recordsData", JSON.stringify(recordsData));
}

async function getUserName(){
    const modal = document.createElement("div");
    modal.className = "user-name";

    const label = document.createElement("label");
    label.innerText = "Your name is worthy of being in records!";

    const div = document.createElement("div");
    const input = document.createElement("input");
    input.autofocus = true;
    input.placeholder = "Name";

    let user;
    if (localStorage.user) {
        try {
            user = JSON.parse(localStorage.getItem("user"));
        } catch (error) {
            console.warn(error);
        }
    }
    if(user){
        input.value = user;
    }
    const button = document.createElement("button");
    button.innerText = "Ok";
    
    div.append(input, button);
    modal.append(label, div);
    page.append(modal);

    return new Promise((resolve) => {
        button.addEventListener("click", () => {
            modal.remove();
            resolve(input.value || "Anonym");
        });
    });
};